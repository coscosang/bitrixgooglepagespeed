<?php
AddEventHandler("main", "OnEndBufferContent", "OnEndBufferContentHandler");
function OnEndBufferContentHandler(&$content)
{
    global $APPLICATION;
    global $USER;
    $dir = $APPLICATION->GetCurDir();
    if ($dir == '/bitrix/admin/' || $_SERVER['REQUEST_METHOD'] == 'POST' || !$_SERVER['HTTP_HOST']) {
        return;
    }

    $arSearch = [];
    $arReplace = [];
    /////////////////////////////////////////////////// LAZY JS
    $GLOBALS['IS_LAZY_JS_ENABLE'] = !$USER->IsAdmin(); //($_GET['optimize']);

    if ($GLOBALS['IS_LAZY_JS_ENABLE']) {
        $arSearch[] = ' src="https://www.youtube.com/embed';
        $arReplace[] = ' class="b-lazy" data-src="https://www.youtube.com/embed';

        $arSearch[] = ' type="text/javascript"';
        $arReplace[] = ' type="rocketlazyloadscript"';

        $arSearch[] = '<script>';
        $arReplace[] = '<script type="rocketlazyloadscript">';
        $arSearch[] = '<script language="JavaScript">';
        $arReplace[] = '<script type="rocketlazyloadscript">';

        $arSearch[] = '<script type="text/javascript">';
        $arReplace[] = '<script type="rocketlazyloadscript">';
        $arSearch[] = '<script type="text/javascript" ';
        $arReplace[] = '<script type="rocketlazyloadscript" ';

        $arSearch[] = '<script data-skip-moving="true">';
        $arReplace[] = '<script type="rocketlazyloadscript">';

        $arSearch[] = '<script async>';
        $arReplace[] = '<script type="rocketlazyloadscript" async>';

        $arSearch[] = '<script  data-skip-moving=true';
        $arReplace[] = '<script type="rocketlazyloadscript"';

        $arSearch[] = '<script data-skip-moving=true';
        $arReplace[] = '<script type="rocketlazyloadscript"';

        $arSearch[] = '<script data-b24-form';
        $arReplace[] = '<script type="rocketlazyloadscript" data-b24-form';
    }
    $script = file_get_contents(__DIR__ . '/lib/lazyloadjavascript.php');
    $arSearch[] = '</body>';
    $arReplace[] = $script . PHP_EOL . '</body>';

    $content = str_replace($arSearch, $arReplace, $content);
}
